package com.example.rawata.demo_2.model;

import android.content.Intent;

import com.example.rawata.demo_2.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rawata on 8/27/2015.
 */
public enum UnFocusTabEnum {

    HOME_PAGE_ICON(R.drawable.ic_action_book),

    MESSAGE_ICON(R.drawable.ic_action_message),

    LOCATION_ICON(R.drawable.ic_action_location),

    SETTING_ICON(R.drawable.ic_action_menu);

    int value;

    static Map<Integer,UnFocusTabEnum> map = new HashMap();

    static {
        map.put(0,HOME_PAGE_ICON);
        map.put(1,MESSAGE_ICON);
        map.put(2,LOCATION_ICON);
        map.put(3,SETTING_ICON);
    }

    private UnFocusTabEnum(int value){
        this.value = value;
    }

    public static int getIcon(int position){
        return map.get(position).value;
    }


}
