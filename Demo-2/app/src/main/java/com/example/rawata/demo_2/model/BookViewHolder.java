package com.example.rawata.demo_2.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rawata.demo_2.R;

public class BookViewHolder extends RecyclerView.ViewHolder {
//  public TextView textView;
  public ImageView imageView;
  public BookViewHolder(View itemView) {
    super(itemView);
//    textView = (TextView) itemView.findViewById(R.id.text);
    imageView = (ImageView) itemView;
  }
}