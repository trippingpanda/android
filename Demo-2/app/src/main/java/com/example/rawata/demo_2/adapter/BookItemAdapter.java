package com.example.rawata.demo_2.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rawata.demo_2.R;
import com.example.rawata.demo_2.activity.BookDetailsActivity;
import com.example.rawata.demo_2.model.BookItem;
import com.example.rawata.demo_2.model.BookViewHolder;
import com.example.rawata.demo_2.util.Utility;

import java.util.ArrayList;
import java.util.List;

public class BookItemAdapter extends RecyclerView.Adapter<BookViewHolder> {
//  private List<String> labels;

  private List<Bitmap> images;

  public BookItemAdapter(List<BookItem> count) {
    /*labels = new ArrayList<String>();
    for (int i = 0; i < count.size(); ++i) {
      labels.add(count.get(i).text);
    }*/

    images = new ArrayList<Bitmap>();
    for (int i = 0; i < count.size(); ++i) {
      images.add(count.get(i).image);
    }
  }

  @Override
  public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item, parent, false);
    ImageView imageView = new ImageView(parent.getContext());
    imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    imageView.setBackground(Utility.getDrawableFromResourceId(R.drawable.back,parent.getContext()));
    return new BookViewHolder(imageView);
  }

  @Override
  public void onBindViewHolder(final BookViewHolder holder, final int position) {
//    final String label = labels.get(position);
//    holder.textView.setText(label);
    holder.imageView.setImageBitmap(images.get(position));
    holder.imageView.setOnClickListener(new View.OnClickListener(){

      @Override
      public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), BookDetailsActivity.class);
        intent.putExtra("title","Book Name");
        v.getContext().startActivity(intent);
      }
    });
//    holder.textView.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        Toast.makeText(
//                holder.textView.getContext(), label, Toast.LENGTH_SHORT).show();
//      }
//    });
  }

  @Override
  public int getItemCount() {
    return images.size();
  }
}