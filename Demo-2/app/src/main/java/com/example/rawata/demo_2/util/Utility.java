package com.example.rawata.demo_2.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by rawata on 8/27/2015.
 */
public class Utility {

    public static Drawable getDrawableFromResourceId(int id,Context context){
        Drawable myDrawable;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            myDrawable = context.getDrawable(id);
        } else {
            myDrawable = context.getResources().getDrawable(id);
        }
        return myDrawable;
    }
}
