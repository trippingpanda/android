package com.example.rawata.demo_2.fragment;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rawata.demo_2.adapter.BookItemAdapter;
import com.example.rawata.demo_2.view.MarginDecoration;
import com.example.rawata.demo_2.R;
import com.example.rawata.demo_2.model.BookItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rawata on 8/25/2015.
 */
public class HomeLayoutFragment extends Fragment {

    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.home_layout,container,false);
        recyclerView = (RecyclerView) v.findViewById(R.id.home_recycler_view);
        recyclerView.addItemDecoration(new MarginDecoration(container.getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new BookItemAdapter(new DownloadImageTask().doInBackground()));
        return v;


    }

    private class DownloadImageTask extends AsyncTask<String,Void,List<BookItem>>{

        @Override
        protected List<BookItem> doInBackground(String... params) {
            List<BookItem> bookItems = new ArrayList<>();
            BitmapFactory.decodeResource(getResources(), R.drawable.bases_of_yoga);

            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.bases_of_yoga, 100, 100), "bases of yoga"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.conversations_with_god,100,100), "conversation with god"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.girl_with_dragon_tattoo,100,100), "girl with dragon tattoo"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.gulliver_travels,100,100),"gulliverTravels"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.liars_poker,100,100),"liar's poker"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.machine_learning,100,100),"machine learning"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.mahabharata,100,100),"mahabharata"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.stay_hungry_stay_foolis,100,100),"stay hungry stay foolisj"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.the_broker,100,100),"the broker"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.the_chose_one,100,100),"the chosen one"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.vikram_seth,100,100),"vikram seth"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.time_machine,100,100),"time machine"));
            bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.secret_of_nagas,100,100),"secret of the nagas"));
//        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.inferno_big,100,100),"secret of the nagas"));

            return bookItems;
        }

    }


    /*private List<BookItem> getData() {
        List<BookItem> bookItems = new ArrayList<>();
        BitmapFactory.decodeResource(getResources(), R.drawable.bases_of_yoga);
        
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.bases_of_yoga, 100, 100), "bases of yoga"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.conversations_with_god,100,100), "conversation with god"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.girl_with_dragon_tattoo,100,100), "girl with dragon tattoo"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.gulliver_travels,100,100),"gulliverTravels"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.liars_poker,100,100),"liar's poker"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.machine_learning,100,100),"machine learning"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.mahabharata,100,100),"mahabharata"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.stay_hungry_stay_foolis,100,100),"stay hungry stay foolisj"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.the_broker,100,100),"the broker"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.the_chose_one,100,100),"the chosen one"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.vikram_seth,100,100),"vikram seth"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(),R.drawable.time_machine,100,100),"time machine"));
        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.secret_of_nagas,100,100),"secret of the nagas"));
//        bookItems.add(new BookItem(decodeSampledBitmapFromResource(getResources(), R.drawable.inferno_big,100,100),"secret of the nagas"));

        return bookItems;
    }*/

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
