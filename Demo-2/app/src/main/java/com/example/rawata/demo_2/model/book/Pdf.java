package com.example.rawata.demo_2.model.book;

/**
 * Created by rawata on 8/28/2015.
 */
public class Pdf {

    public boolean isAvailable;

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }
}
