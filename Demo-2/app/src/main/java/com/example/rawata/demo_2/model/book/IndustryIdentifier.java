package com.example.rawata.demo_2.model.book;

/**
 * Created by rawata on 8/28/2015.
 */
public class IndustryIdentifier
{
    public String type;

    public String identifier;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
