package com.example.rawata.demo_2.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.rawata.demo_2.R;
import com.example.rawata.demo_2.model.book.RootObject;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * Created by rawata on 8/28/2015.
 */
public class AddBookFragment extends Fragment {

    RootObject rootObject;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_book_layout, container, false);

        final EditText isbn = (EditText) v.findViewById(R.id.isbn);

        final String[] asd = {null};

        v.findViewById(R.id.findBookButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 rootObject = new BookFindTask().doInBackground(isbn.getText().toString());
            }
        });

        return v;
    }

    private class BookFindTask extends AsyncTask<String,Integer,RootObject>{

        @Override
        protected RootObject doInBackground(String... params) {
            String isbnString = params[0];
            String uri = "https://www.googleapis.com/books/v1/volumes?q=isbn:"+isbnString;

            HttpClient client = new DefaultHttpClient();

            HttpGet httpGet = new HttpGet(uri);
            try {
                HttpResponse response = client.execute(httpGet);
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(response.getEntity().getContent(), RootObject.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(RootObject rootObject) {
            super.onPostExecute(rootObject);
        }
    }


}
