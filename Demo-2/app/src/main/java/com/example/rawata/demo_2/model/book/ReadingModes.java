package com.example.rawata.demo_2.model.book;

/**
 * Created by rawata on 8/28/2015.
 */
public class ReadingModes {

    public boolean text;

    public boolean image;

    public boolean isText() {
        return text;
    }

    public void setText(boolean text) {
        this.text = text;
    }

    public boolean isImage() {
        return image;
    }

    public void setImage(boolean image) {
        this.image = image;
    }
}
