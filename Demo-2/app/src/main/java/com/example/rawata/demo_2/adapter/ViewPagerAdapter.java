package com.example.rawata.demo_2.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.example.rawata.demo_2.fragment.AddBookFragment;
import com.example.rawata.demo_2.fragment.HomeLayoutFragment;
import com.example.rawata.demo_2.fragment.LocationFragment;
import com.example.rawata.demo_2.fragment.MessageFragment;
import com.example.rawata.demo_2.fragment.SettingsFragment;

/**
 * Created by hp1 on 21-01-2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public int[] getTitles() {
        return titles;
    }

    public void setTitles(int[] titles) {
        this.titles = titles;
    }

    int titles[]; // This will Store the titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

    Context context;


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm,int mTitles[], int mNumbOfTabsumb,Context context) {
        super(fm);
        this.context=context;
        this.titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                HomeLayoutFragment tab1 = new HomeLayoutFragment();
                return tab1;
            case 1:
                MessageFragment tab2 = new MessageFragment();
                return tab2;
            case 2:
                LocationFragment tab3 = new LocationFragment();
                return tab3;
            case 3:
                AddBookFragment tab4 = new AddBookFragment();
                return tab4;
            case 4:
                SettingsFragment tab5 = new SettingsFragment();
                return tab5;
        }

        return null;

    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable myDrawable;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            myDrawable = context.getDrawable(titles[position]);
        } else {
            myDrawable = context.getResources().getDrawable(titles[position]);
        }
        myDrawable.setBounds(0,0,40,40);
        ImageSpan imageSpan = new ImageSpan(myDrawable);

        SpannableString spannableString = new SpannableString("asd");
        spannableString.setSpan(imageSpan,0,spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }


    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}