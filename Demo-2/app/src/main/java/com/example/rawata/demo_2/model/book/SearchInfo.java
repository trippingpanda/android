package com.example.rawata.demo_2.model.book;

/**
 * Created by rawata on 8/28/2015.
 */
public class SearchInfo {

    public String textSnippet;

    public String getTextSnippet() {
        return textSnippet;
    }

    public void setTextSnippet(String textSnippet) {
        this.textSnippet = textSnippet;
    }
}
