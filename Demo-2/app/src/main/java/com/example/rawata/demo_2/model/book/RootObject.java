package com.example.rawata.demo_2.model.book;

import java.util.List;

/**
 * Created by rawata on 8/28/2015.
 */
public class RootObject {

    public String kind;

    public int totalItems;

    public List<Item> items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
