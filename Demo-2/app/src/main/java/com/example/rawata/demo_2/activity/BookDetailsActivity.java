package com.example.rawata.demo_2.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.rawata.demo_2.R;

/**
 * Created by rawata on 8/28/2015.
 */
public class BookDetailsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_details_layout);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            String title = extras.getString("title");
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle(title);


        }
    }
}
