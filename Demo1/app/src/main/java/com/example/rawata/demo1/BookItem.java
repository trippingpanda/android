package com.example.rawata.demo1;

import android.graphics.Bitmap;

/**
 * Created by rawata on 8/25/2015.
 */
public class BookItem {
    Bitmap image;

    String text;

    public BookItem(Bitmap image,String text) {
       this.image = image;
        this.text = text;
    }
}
