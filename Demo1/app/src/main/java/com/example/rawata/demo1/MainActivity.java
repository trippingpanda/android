package com.example.rawata.demo1;

import android.app.Fragment;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.addItemDecoration(new MarginDecoration(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new BookItemAdapter( getData()));
    }

    private List<BookItem> getData() {
        List<BookItem> bookItems = new ArrayList<>();
        /*bookItems.add(new BookItem("bases of yoga"));

        bookItems.add(new BookItem("bases of yoga"));

        bookItems.add(new BookItem("bases of yoga"));
        bookItems.add(new BookItem("bases of yoga"));
        bookItems.add(new BookItem("bases of yoga"));
        bookItems.add(new BookItem("bases of yoga"));*/



        BitmapFactory.decodeResource(getResources(), R.drawable.bases_of_yoga);

        bookItems.add(new BookItem((BitmapFactory.decodeResource(getResources(),R.drawable.bases_of_yoga)),"bases of yoga"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.conversations_with_god))),"conversation with god"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.girl_with_dragon_tattoo))),"girl with dragon tattoo"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.gulliver_travels))),"gulliverTravels"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.liars_poker))),"liar's poker"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.machine_learning))),"machine learning"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.mahabharata))),"mahabharata"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.stay_hungry_stay_foolis))),"stay hungry stay foolisj"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.the_broker))),"the broker"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.the_chose_one))),"the chosen one"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.vikram_seth))),"vikram seth"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(),R.drawable.time_machine))),"time machine"));
        bookItems.add(new BookItem(((BitmapFactory.decodeResource(getResources(), R.drawable.secret_of_nagas))),"secret of the nagas"));





        return bookItems;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar book_item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
