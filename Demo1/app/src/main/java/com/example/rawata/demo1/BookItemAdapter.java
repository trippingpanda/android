package com.example.rawata.demo1;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BookItemAdapter extends RecyclerView.Adapter<BookViewHolder> {
  private List<String> labels;

  private List<Bitmap> images;

  public BookItemAdapter(List<BookItem> count) {
    labels = new ArrayList<String>();
    for (int i = 0; i < count.size(); ++i) {
      labels.add(count.get(i).text);
    }

    images = new ArrayList<Bitmap>();
    for (int i = 0; i < count.size(); ++i) {
      images.add(count.get(i).image);
    }
  }

  @Override
  public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item, parent, false);
    return new BookViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final BookViewHolder holder, final int position) {
    final String label = labels.get(position);
    holder.textView.setText(label);
    holder.imageView.setImageBitmap(images.get(position));
    holder.textView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(
                holder.textView.getContext(), label, Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public int getItemCount() {
    return labels.size();
  }
}