package com.example.rawata.demo1;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BookViewHolder extends RecyclerView.ViewHolder {
  public TextView textView;
  public ImageView imageView;
  public BookViewHolder(View itemView) {
    super(itemView);
    textView = (TextView) itemView.findViewById(R.id.text);
    imageView = (ImageView) itemView.findViewById(R.id.imageView);
  }
}